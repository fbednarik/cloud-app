package sk.drndos.cloud.sampleservice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SampleController {

    private final SampleService sampleService;

    public SampleController(SampleService sampleService) {
        this.sampleService = sampleService;
    }

    @GetMapping("/sample/home")
    public String home() {
        return "home";
    }

    @GetMapping("/sample/complicated")
    @ResponseBody
    public String complicated() {
        return sampleService.getComplicatedStuff();
    }
}
