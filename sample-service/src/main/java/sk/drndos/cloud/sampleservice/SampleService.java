package sk.drndos.cloud.sampleservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @author Filip Bednárik
 * @since 21. 8. 2018
 */
@Service
public class SampleService {

        @HystrixCommand(fallbackMethod = "defaultComplicatedStuff")
        public String getComplicatedStuff() {
            if(new Random().nextInt(2) == 1){
                throw new RuntimeException("Failed");
            }
            return "this is complicated";
        }

        public String defaultComplicatedStuff() {
            return "not so complicated";
        }
}
