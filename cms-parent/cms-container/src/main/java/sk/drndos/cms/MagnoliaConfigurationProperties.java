package sk.drndos.cms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="conf.magnolia")
@Data
public class MagnoliaConfigurationProperties {

    private Datasource datasource;

    @Data
    public static class Datasource {
        private String url;
        private String user;
        private String password;
    }
}
