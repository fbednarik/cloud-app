package sk.drndos.cms;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;

import javax.servlet.ServletException;
import java.io.File;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {SpringApplicationAdminJmxAutoConfiguration.class})
public class MagnoliaSpringBootApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MagnoliaSpringBootApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MagnoliaSpringBootApplication.class);
    }

    private void setAllKnownMagnoliaProperties(Environment env) {
        if (env instanceof ConfigurableEnvironment) {
            for (PropertySource<?> propertySource : ((ConfigurableEnvironment) env).getPropertySources()) {
                if (propertySource instanceof EnumerablePropertySource) {
                    for (String key : ((EnumerablePropertySource) propertySource).getPropertyNames()) {
                        if (key.startsWith("conf.magnolia")) {
                            System.setProperty(key, propertySource.getProperty(key).toString());
                        }
                    }
                }
            }
        }
    }

    @Bean
    public ServletWebServerFactory servletContainerFactory(Environment environment) {
        setAllKnownMagnoliaProperties(environment);
        return new TomcatServletWebServerFactory() {
            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                try {
                    String webappDirLocation = "./cms-parent/cms-webapp/target";
                    tomcat.getHost().getAppBaseFile().mkdir();
                    /*tomcat.addWebapp("/magnoliaAuthor", new
                            File(webappDirLocation + "/magnolia-spring-boot-webapp-1.0-SNAPSHOT.war").getAbsolutePath());*/
                    Context ctx = tomcat.addWebapp(
                            "/magnoliaPublic",
                            new File(webappDirLocation + "/cms-webapp-1.0-SNAPSHOT.war").getAbsolutePath()
                    );
                } catch (ServletException ex) {
                    throw new IllegalStateException("Failed to add webapp", ex);
                }
                return super.getTomcatWebServer(tomcat);
            }

        };
    }

}
