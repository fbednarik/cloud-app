package sk.drndos.cloud.sampleservice.pages;

import org.apache.wicket.markup.html.GenericWebPage;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import de.agilecoders.wicket.core.markup.html.bootstrap.image.GlyphIconType;
import de.agilecoders.wicket.core.markup.html.bootstrap.navbar.Navbar;
import de.agilecoders.wicket.core.markup.html.bootstrap.navbar.NavbarButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.navbar.NavbarComponents;

public class BasePage extends GenericWebPage<Void> {

    public BasePage(final PageParameters parameters) {
        super(parameters);

        add(newNavbar("navbar"));
    }

    protected Navbar newNavbar(String markupId) {
        Navbar navbar = new Navbar(markupId);

        navbar.setPosition(Navbar.Position.TOP);
        navbar.setInverted(true);

        navbar.setBrandName(Model.of("Hello"));

        navbar.addComponents(NavbarComponents.transform(Navbar.ComponentPosition.LEFT,
                new NavbarButton<Void>(HomePage.class, Model.of("Home"))
                        .setIconType(GlyphIconType.home),
                new NavbarButton<Void>(FirstPage.class, Model.of("First page")),
                new NavbarButton<Void>(UserAccountPage.class, Model.of("Profile page")),
                new NavbarButton<Void>(SecondPage.class, Model.of("Second page"))
        ));


        return navbar;
    }

}
