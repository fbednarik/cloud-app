package sk.drndos.cloud.sampleservice.pages;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("first")
public class FirstPage extends BasePage {

    public FirstPage(PageParameters parameters) {
        super(parameters);
    }
}
