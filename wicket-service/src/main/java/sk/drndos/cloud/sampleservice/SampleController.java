package sk.drndos.cloud.sampleservice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SampleController {
    @GetMapping("/sample/home")
    public String home() {
        return "home";
    }
}
