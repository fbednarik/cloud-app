package sk.drndos.cloud.sampleservice.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.wicket.util.io.IClusterable;

/**
 * @author Filip Bednárik
 * @since 15.6.2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements IClusterable {
    private String name;
    private String email;
}
