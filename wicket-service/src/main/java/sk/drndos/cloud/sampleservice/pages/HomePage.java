package sk.drndos.cloud.sampleservice.pages;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;

@WicketHomePage
@MountPath("home")
public class HomePage extends BasePage {
    public HomePage(PageParameters parameters) {
        super(parameters);
    }
}
