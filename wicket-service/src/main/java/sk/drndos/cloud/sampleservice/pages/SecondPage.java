package sk.drndos.cloud.sampleservice.pages;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("second")
public class SecondPage extends BasePage {

    public SecondPage(PageParameters parameters) {
        super(parameters);
    }
}