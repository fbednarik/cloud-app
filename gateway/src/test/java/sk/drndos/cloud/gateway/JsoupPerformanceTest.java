package sk.drndos.cloud.gateway;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.HtmlTreeBuilder;
import org.jsoup.parser.Parser;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JsoupPerformanceTest {

    @Rule
    public MyJUnitStopWatch stopwatch = new MyJUnitStopWatch();

    private static Document document4;
    private static Document document2;
    private static Document document3;
    private static Document document1;
    private static Document document;


    @BeforeClass
    public static void beforeAll() throws IOException {
        document4 = Jsoup.parse(JsoupPerformanceTest.class.getClass().getResourceAsStream("test4.html"), "UTF-8", "http://drndos.sk/");
        document2 = Jsoup.parse(JsoupPerformanceTest.class.getClass().getResourceAsStream("test2.html"), "UTF-8", "http://drndos.sk/");
        document1 = Jsoup.parse(JsoupPerformanceTest.class.getClass().getResourceAsStream("test1.html"), "UTF-8", "http://drndos.sk/");
        document3 = Jsoup.parse(JsoupPerformanceTest.class.getClass().getResourceAsStream("test3.html"), "UTF-8", "http://drndos.sk/");
        document = Jsoup.parse(JsoupPerformanceTest.class.getClass().getResourceAsStream("test.html"), "UTF-8", "http://drndos.sk/");

    }

    @Test
    public void jsoupIdSelectorTest() throws IOException {
        document4.select("#data-loc").size();
    }

    @Test
    public void jsoupClassSelectorTest() throws IOException {
        document2.select(".data-loc").size();
    }

    @Test
    public void jsoupDivClassSelectorTest() throws IOException {
        document1.select("div.data-loc").size();
    }

    @Test
    public void jsoupAttributeSelectorTest() throws IOException {
        document3.select("div[data-loc]").size();
    }

    @Test
    public void aaa() throws IOException {
        document.select("div.jumbotron");
    }
}
