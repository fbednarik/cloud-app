package sk.drndos.cloud.sampleservice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LayoutController {
    @GetMapping("/layout/index")
    public String home() {
        return "index";
    }
}
